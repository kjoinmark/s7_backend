from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_user_check_input_valid_output_false():
    response = client.get("/user_check/?name1=Aaa&name2=Bbb")
    assert response.status_code == 200
    assert response.json() == False


def test_user_check_input_valid_output_true():
    response = client.get("/user_check/?name1=Aaa&name2=Aaa")
    assert response.status_code == 200
    assert response.json() == True


def test_user_check_input_without_required_arg_output_exception500():
    response = client.get("/user_check/")
    assert response.status_code == 500
    assert response.json() == {'detail': 'Incorrect input'}


def test_user_check_input_with_empty_arg_output_exception500():
    response = client.get("/user_check/?name1=&name2=")
    assert response.status_code == 500
    assert response.json() == {'detail': 'Empty input'}


def test_user_check_input_only_first_arg_output_exception500():
    response = client.get("/user_check/?name1=Aaa")
    assert response.status_code == 500
    assert response.json() == {'detail': 'Incorrect input'}


def test_user_check_input_only_second_arg_output_exception500():
    response = client.get("/user_check/?name2=Aaa")
    assert response.status_code == 500
    assert response.json() == {'detail': 'Incorrect input'}
