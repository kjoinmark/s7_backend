import app.utils as utils


def test_check_names_same():
    assert utils.check_names('same', 'same') == True
    assert utils.check_names('sAMe', 'sAMe') == True
    assert utils.check_names(' same', ' same') == True
    assert utils.check_names('same', ' same') == True
    assert utils.check_names('sAMe', 'same') == True
    assert utils.check_names(' same', ' same  ') == True


def test_check_names_not_same():
    assert utils.check_names('Not same', 'same') == False
    assert utils.check_names('Notsame', 'same') == False
    assert utils.check_names('Not same', 'same Not') == False
