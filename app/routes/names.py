from typing import Union

from fastapi import APIRouter, Query
from typing_extensions import Annotated

from .. import utils
from ..exceptions import exceptions

router = APIRouter()


@router.get("/user_check/")
async def check_names(name1: Annotated[Union[str, None], Query(max_length=50)] = None,
                      name2: Annotated[Union[str, None], Query(max_length=50)] = None):
    if name1 is None or name2 is None:
        raise exceptions.InputHasNone500()

    if len(name1) == 0 or len(name2) == 0:
        raise exceptions.InputHasEmpty500()

    return utils.check_names(name1, name2)
