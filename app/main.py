from fastapi import FastAPI
from .routes import names

app = FastAPI()

app.include_router(names.router)


@app.get("/")
async def index():
    return {'status': 200, 'message': 'OK'}
