from fastapi import HTTPException


def InputHasNone500():
    return HTTPException(500, "Incorrect input")


def InputHasEmpty500():
    return HTTPException(500, "Empty input")
