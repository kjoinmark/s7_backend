FROM python:3.9

COPY ./app ./app

RUN pip install "fastapi[all]"

EXPOSE 8000

CMD sh app/run.sh